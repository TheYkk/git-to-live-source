#!/usr/bin/env bash

set -e

export GOOGLE_APPLICATION_CREDENTIALS=/.secrets/key-file.json

mkdir -p /.secrets
echo $GCLOUD_KEY_FILE > "$GOOGLE_APPLICATION_CREDENTIALS"

set -x

if [ -z "$GL_IMAGE_TAG" ]; then
  echo "No GL_IMAGE_TAG"
  exit 1
fi

if [ -z "$CI_BUILD_REF_NAME" ]; then
  echo "No CI_BUILD_REF_NAME"
  exit 1
fi

if [ -z "$CI_PROJECT_NAME" ]; then
  echo "No CI_PROJECT_NAME"
  exit 1
fi

if [ -z "$CI_REGISTRY" ]; then
  echo "No CI_REGISTRY"
  exit 1
fi

PORT=${PORT:-3000}
KUBE_ENV=${KUBE_ENV:-dev}
KUBE_NAME=${KUBE_NAME:-hello-$CI_BUILD_REF_NAME}

ROOT="$(dirname "${BASH_SOURCE[0]}")"
mkdir -p "$ROOT/.generated"

for f1 in "$ROOT"/deploy-files/*.yaml
do
  envsubst < "$f1" > "$ROOT/.generated/$(basename $f1)"
done

if [ -n "$GL_ENV_URL" ]; then
  for f2 in "$ROOT"/deploy-files/server/*.yaml
  do
    envsubst < "$f2" > "$ROOT/.generated/$(basename $f2)"
  done
fi

gcloud auth activate-service-account \
  --key-file="/.secrets/key-file.json"
gcloud container clusters get-credentials kublr \
  --zone europe-west1-c --project kublr-158610

for f3 in "$ROOT/.generated"/*;
do
  cat "${f3}"
done

kubectl $1 -f "$ROOT/.generated/"
